const User = require("../models/users");
const Room = require("../models/rooms");
const Chats = require("../models/messages");

class Chat {
  async join(req, res,next) {
    try {
      //input user and room
      const { username, roomId } = req.body;

      const userExist=await User.findOne({username})
      const roomExist=await Room.findOne({name:roomId})
      if(!userExist){
        await User.create({ username });
      }
      if(!roomExist){
        await Room.create({ name: roomId });
      }
      

      return res.json({
        message: "success",
        code: 200,
        result: {
          users: username,
          room: roomId,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  async chatRoom(req,res){
      const {username,room}=req.query
  }

  async newMessage(data) {
    try {
      const { username, roomId } = data;
      const { message } = req.body;
      const newMessage = await Chats.create({
        username: username,
        room: roomId,
        message,
      });
      return 

 
    } catch (error) {
        console.log(error)
    }
  }

  async showMessages(room){
    const messages=await Chats.find({room})

    return messages
  }
}

module.exports = Object.freeze(new Chat());

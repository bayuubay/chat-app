const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ChatSchema = new Schema({
  username: {
    type: String,
  },

  room: {
    type: String,
  },

  message: {
    type: String,
  },
});

module.exports = mongoose.model("chats", ChatSchema);

const router = require("express").Router();

const chatController = require("./controllers/chat");

router.get("/", (req, res) => {
  res.send({ response: "Server is up and running." }).status(200);
});

router.post("/join", /*validator,*/ chatController.join);

router.post("/chat", chatController.newMessage);

module.exports = router;
